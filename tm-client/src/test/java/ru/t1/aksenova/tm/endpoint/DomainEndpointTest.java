package ru.t1.aksenova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.aksenova.tm.api.endpoint.IDomainEndpoint;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.dto.request.*;
import ru.t1.aksenova.tm.marker.IntegrationCategory;
import ru.t1.aksenova.tm.service.PropertyService;

import static ru.t1.aksenova.tm.constant.UserTestData.ADMIN_LOGIN;
import static ru.t1.aksenova.tm.constant.UserTestData.ADMIN_PASSWORD;
@Ignore
@Category(IntegrationCategory.class)
public class DomainEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IDomainEndpoint domainEndpointClient = IDomainEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_LOGIN);
        loginRequest.setPassword(ADMIN_PASSWORD);
        adminToken = authEndpointClient.login(loginRequest).getToken();
    }

    @AfterClass
    public static void tearDown() {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(adminToken);
        authEndpointClient.logout(request);
    }

    @Test
    public void backupDataLoad() {
        Assert.assertNotNull(adminToken);
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.backupDataLoad(request));
    }

    @Test
    public void backupDataSave() {
        Assert.assertNotNull(adminToken);
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.backupDataSave(request));
    }

    @Test
    public void loadDataBase64() {
        Assert.assertNotNull(adminToken);
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataBase64(request));
    }

    @Test
    public void saveDataBase64() {
        Assert.assertNotNull(adminToken);
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataBase64(request));
    }

    @Test
    public void loadDataBinary() {
        Assert.assertNotNull(adminToken);
        @NotNull final DataBinaryLoadRequest request = new DataBinaryLoadRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataBinary(request));
    }

    @Test
    public void saveDataBinary() {
        Assert.assertNotNull(adminToken);
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataBinary(request));
    }

    @Test
    public void loadDataJsonFasterXml() {
        Assert.assertNotNull(adminToken);
        @NotNull final DataJsonLoadFasterXmlRequest request = new DataJsonLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataJsonFasterXml(request));
    }

    @Test
    public void saveDataJsonFasterXml() {
        Assert.assertNotNull(adminToken);
        @NotNull final DataJsonSaveFasterXmlRequest request = new DataJsonSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataJsonFasterXml(request));
    }

    @Test
    public void loadDataJsonJaxb() {
        Assert.assertNotNull(adminToken);
        @NotNull final DataJsonLoadJaxbRequest request = new DataJsonLoadJaxbRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataJsonJaxb(request));
    }

    @Test
    public void saveDataJsonJaxb() {
        Assert.assertNotNull(adminToken);
        @NotNull final DataJsonSaveJaxbRequest request = new DataJsonSaveJaxbRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataJsonJaxb(request));
    }

    @Test
    public void loadDataXmlFasterXml() {
        Assert.assertNotNull(adminToken);
        @NotNull final DataXmlLoadFasterXmlRequest request = new DataXmlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataXmlFasterXml(request));
    }

    @Test
    public void saveDataXmlFasterXml() {
        Assert.assertNotNull(adminToken);
        @NotNull final DataXmlSaveFasterXmlRequest request = new DataXmlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataXmlFasterXml(request));
    }

    @Test
    public void loadDataXmlJaxb() {
        Assert.assertNotNull(adminToken);
        @NotNull final DataXmlLoadJaxbRequest request = new DataXmlLoadJaxbRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataXmlJaxb(request));
    }

    @Test
    public void saveDataXmlJaxb() {
        Assert.assertNotNull(adminToken);
        @NotNull final DataXmlSaveJaxbRequest request = new DataXmlSaveJaxbRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataXmlJaxb(request));
    }

    @Test
    public void loadDataYamlFasterXml() {
        Assert.assertNotNull(adminToken);
        @NotNull final DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataYamlFasterXml(request));
    }

    @Test
    public void saveDataYamlFasterXml() {
        Assert.assertNotNull(adminToken);
        @NotNull final DataYamlSaveFasterXmlRequest request = new DataYamlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataYamlFasterXml(request));
    }

}
