package ru.t1.aksenova.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.enumerated.TaskSort;
import ru.t1.aksenova.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @NotNull
    Task add(@Nullable Task task);

    @NotNull
    Task add(@Nullable String userId, @Nullable Task task);

    @NotNull
    @SneakyThrows
    Collection<Task> set(@NotNull Collection<Task> tasks);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@Nullable String userId);

    @NotNull
    List<Task> findAll(@Nullable String userId, @Nullable TaskSort sort);

    @Nullable
    Task findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task findOneByIndex(@Nullable String userId, @Nullable Integer index);


    void clear();

    void removeAll(@Nullable String userId);

    @Nullable
    Task remove(@Nullable String userId, @Nullable Task task);

    @Nullable
    Task removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @SneakyThrows
    boolean existsById(@Nullable String userId, @Nullable String id);

    @SneakyThrows
    long getSize(@Nullable String userId);

    @NotNull
    Task updateProjectIdById(@Nullable String userId, @Nullable String id, @Nullable String project_id);

    @NotNull
    Task updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    List<Task> findAll(@Nullable String userId, @Nullable Comparator<Task> comparator);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

}
