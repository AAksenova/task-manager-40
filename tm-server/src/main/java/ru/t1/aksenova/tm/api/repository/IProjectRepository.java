package ru.t1.aksenova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, created, name, description, status, user_id) " +
            "VALUES (#{project.id}, #{project.created}, #{project.name}, #{project.description}, " +
            "#{project.status}, #{project.userId})")
    void add(@NotNull @Param("project") Project project);

    @Insert("INSERT INTO tm_project (id, created, name, description, user_id, status) " +
            "VALUES (#{project.id}, #{project.created}, #{project.name}, #{project.description}, " +
            "#{userId}, #{project.status})")
    void addWithUserId(@NotNull @Param("userId") String userId, @NotNull @Param("project") Project project);

    @Delete("DELETE FROM tm_project where user_id = #{userId}")
    void removeAll(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_project")
    void clear();

    @Delete("DELETE FROM tm_project where user_id = #{userId} AND id = #{project.id}")
    void removeOne(@NotNull @Param("userId") String userId, @NotNull @Param("project") Project project);

    @Select("SELECT * FROM tm_project")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Project> findAll();

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Project> findAllByUserId(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Project> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Project> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Project> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Project findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Project findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(1) FROM tm_project WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Update("UPDATE tm_project SET name = #{project.name}, description = #{project.description}, status = #{project.status} " +
            "WHERE id = #{project.id} ")
    void update(@NotNull @Param("project") Project project);

}
