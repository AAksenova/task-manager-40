package ru.t1.aksenova.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.service.DomainService;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void save() {
        bootstrap.getDomainService().dataBackupSave();
    }

    public void stop() {
        es.shutdown();
    }

    public void load() {
        @NotNull final String fileName = DomainService.FILE_BACKUP;
        if (!Files.exists(Paths.get(fileName))) return;
        bootstrap.getDomainService().dataBackupLoad();
    }

}
