package ru.t1.aksenova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.model.Session;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (id, user_id, date, role) " +
            "VALUES (#{session.id}, #{session.userId}, #{session.date}, #{session.role})")
    void add(@NotNull @Param("session") Session session);

    @Delete("DELETE FROM tm_session where user_id = #{userId}")
    void removeAll(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_session")
    void clear();

    @Delete("DELETE FROM tm_session where id = #{session.id}")
    void removeOne(@NotNull @Param("session") Session session);

    @Select("SELECT * FROM tm_session where user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Session> findAll(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_session WHERE id = #{id} and user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Session findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT COUNT(1) FROM tm_session where user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Update("UPDATE tm_session SET date = #{session.date}, role = #{session.role} WHERE id = #{session.id} ")
    void update(@NotNull @Param("session") Session session);

}
