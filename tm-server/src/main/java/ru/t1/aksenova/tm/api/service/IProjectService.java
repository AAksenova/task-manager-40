package ru.t1.aksenova.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.enumerated.ProjectSort;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.model.Project;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @NotNull
    Project add(@Nullable Project model);

    @NotNull
    Project add(@Nullable String userId, @Nullable Project model);

    @NotNull
    @SneakyThrows
    Collection<Project> set(@NotNull Collection<Project> projects);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@Nullable String userId);

    @NotNull
    List<Project> findAll(@Nullable String userId, @Nullable ProjectSort sort);

    @NotNull
    List<Project> findAll(@Nullable String userId, @Nullable Comparator<Project> comparator);

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @SneakyThrows
    void clear();

    @SneakyThrows
    void removeAll(@Nullable String userId);

    @Nullable
    Project remove(@Nullable String userId, @Nullable Project project);

    @Nullable
    Project removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Project updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    boolean existsById(@Nullable String userId, @Nullable String id);

    long getSize(@Nullable String userId);
}
