package ru.t1.aksenova.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class DataJsonLoadJaxbRequest extends AbstractUserRequest {

    public DataJsonLoadJaxbRequest(@Nullable final String token) {
        super(token);
    }

}
